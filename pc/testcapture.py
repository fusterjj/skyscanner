#!/usr/bin/env python3

import cv2
import struct

cap = cv2.VideoCapture(1)
if not cap.isOpened():
    print('Error opening device')
    exit(-1)

fourcc_desired = 'YUYV'
fourcc_desired_int = struct.unpack('I', fourcc_desired.encode('ascii'))[0]

#cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
#cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
cap.set(cv2.CAP_PROP_FOURCC, fourcc_desired_int)
#cap.set(cv2.CAP_PROP_CONVERT_RGB, 1)


print('Convert RGB: {:}'.format(cap.get(cv2.CAP_PROP_CONVERT_RGB)))
print('Format: {:}'.format(cap.get(cv2.CAP_PROP_FORMAT)))
fourcc_int = int(cap.get(cv2.CAP_PROP_FOURCC))
fourcc = struct.pack('I', fourcc_int).decode('ascii')
print('fourcc: {:}'.format(fourcc))
print('Height: {:}'.format(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
print('Width: {:}'.format(cap.get(cv2.CAP_PROP_FRAME_WIDTH)))
print('FPS: {:}'.format(cap.get(cv2.CAP_PROP_FPS)))

r,frame = cap.read()
cap.release()

cv2.imwrite('test.jpg', frame)

