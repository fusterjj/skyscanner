#!/usr/bin/env python3
import cv2
import cv2.aruco as aruco

dictionary = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)
board = aruco.CharucoBoard_create(4,4,.025,.0125,dictionary)
img = board.draw((200*4,200*4))

#Dump the calibration board to a file
cv2.imwrite('charuco_cal_board.png',img)

