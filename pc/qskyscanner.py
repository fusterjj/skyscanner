#!/usr/bin/env python3

import sys
import time
import signal
import subprocess
import glob
from PySide.QtGui import QApplication, QMainWindow

from skyscanner_ui import Ui_MainWindow
import skyscanner

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

class TeamControl:
    def __init__(self):
        self.name = ''
        self.process = None

    def init(self, name, device_num, input_record=False, debug=False):
        self.name = name
        self.device_num = device_num
        self.input_record_filename = ''
        self.debug = debug
        self.process = None
        print('QSS: Init {0}'.format(self.name))
        print('device_num: {0}, input_record: {1}, debug: {2}'.format(device_num, input_record, debug))
        #args = ['gnome-terminal', '-x', 'sh', '-c', './skyscanner.py']
        args = ['./skyscanner.py']
        args.extend(['--device', str(self.device_num), '-n', self.name])
        if input_record:
            timestring = time.asctime().replace(' ','_').replace(':','')
            self.input_record_filename = name.replace(' ','_') + '_' + timestring + '.avi'
            args.extend(['-r', self.input_record_filename])
            print('QSS: Recording to {0}'.format(self.input_record_filename))

        if self.debug:
            args.append('-d')

        print(args)
        #self.process = Process(target=skyscanner.main, args=(args,))
        self.process = subprocess.Popen(args)

    def start(self):
        print('QSS: Start {0}'.format(self.name))
        if self.process is not None:
            self.process.send_signal(signal.SIGUSR1)

    def reset(self):
        print('QSS: Reset {0}'.format(self.name))
        if self.process is not None:
            self.process.send_signal(signal.SIGUSR2)

    def kill(self):
        print('QSS: Kill {0}'.format(self.name))
        if self.process is not None:
            self.process.send_signal(signal.SIGINT)
            #sleep(0.5)
            #self.process.terminate()
            self.process = None

def startBoth():
    global team1
    global team2
    team1.start()
    team2.start()
    
def resetBoth():
    global team1
    global team2
    team1.reset()
    team2.reset()

def killBoth():
    global team1
    global team2
    team1.kill()
    team2.kill()

def main():
    global team1
    global team2

    team1 = TeamControl()
    team2 = TeamControl()
    app = QApplication(sys.argv)


    # Default to last two devices
    video_devs = glob.glob('/dev/video?')
    if len(video_devs) > 1:
        team2_device = len(video_devs)-1
        team1_device = len(video_devs)-2
    elif len(video_devs) > 0:
        team1_device = len(video_devs)-1
        team2_device = None
    else:
        print('Error, no video devices found')
        exit(-1)

    print('Team 1 device {:}'.format(team1_device))
    print('Team 2 device {:}'.format(team2_device))

    mainwin = MainWindow()
    mainwin.setupUi(mainwin)
    mainwin.pb_init_team1.clicked.connect(
            lambda team=team1 : team.init(
                mainwin.le_team1_name.text(), 
                team1_device, 
                input_record=mainwin.cb_recordin1.isChecked(),
                debug=mainwin.cb_debug1.isChecked()))
    if team2_device is not None:
        mainwin.pb_init_team2.clicked.connect(
                lambda team=team2 : team.init(
                    mainwin.le_team2_name.text(), 
                    team2_device, 
                    debug=mainwin.cb_debug2.isChecked()))
    else:
        mainwin.pb_init_team2.setEnabled(False)
        mainwin.pb_start_team2.setEnabled(False)
        mainwin.pb_reset_team2.setEnabled(False)

    mainwin.pb_start_team1.clicked.connect(lambda team=team1 : team.start())
    mainwin.pb_start_team2.clicked.connect(lambda team=team2 : team.start())
    mainwin.pb_reset_team1.clicked.connect(lambda team=team1 : team.reset())
    mainwin.pb_reset_team2.clicked.connect(lambda team=team2 : team.reset())
    mainwin.pb_start_both.clicked.connect(startBoth)
    mainwin.pb_reset_both.clicked.connect(resetBoth)
    mainwin.pb_kill_both.clicked.connect(killBoth)

    mainwin.show()
    app.exec_()

if __name__ == '__main__':
    main()
