#!/usr/bin/env python3
"""
Copyright (C) 2017 Joel Fuster - All rights reserved
"""

import time
import signal
import cv2.aruco as aruco
import cv2
#import zbarlight
import struct
import pickle
import numpy as np
from collections import Counter
from fcntl import ioctl
from multiprocessing import Process, Queue, Value
from queue import Empty

V4L2_STD_NTSC_M = b'\x00\x10\x00\x00'
V4L2_INPUT_0 = b'\x00\x00\x00\x00'
V4L2_BUF_TYPE_VIDEO_CAPTURE = 1
V4L2_FIELD_INTERLACED = 4
V4L2_VIDIOC_S_INPUT = 0xc0045627
V4L2_VIDIOC_S_STD = 0x40085618 
V4L2_VIDIOC_S_FMT = 0xc0d05605
FRAME_WIDTH = 640
FRAME_HEIGHT = 480 

(CODE_TYPE_QR, CODE_TYPE_ARUCO_4X4, CODE_TYPE_ARUCO_5X5, CODE_TYPE_ARUCO_4X4_CUSTOM) = range(4)

class Scanner:
    def __init__(self, device, outputrawfile, fourcc='YUYV', code_type=CODE_TYPE_ARUCO_4X4, 
            cal_filename='camera_calibration.p', interlaced=False,
            inputvideofile='', window_frames=3, frame_th=2, debug=False):
        self.device = device
        self.fourcc = fourcc
        self.code_type = code_type
        self.cal_filename = cal_filename
        self.output_raw_video = None
        self.interlaced = interlaced
        self.field_even = True 
        self.scan_window = []
        self.frame_th = frame_th
        self.window_frames = window_frames
        self.debug = debug
        self.eof = Value('i', 0)
        self.rawImageQueue = Queue()
        self.imageReadProcess = Process(target=self.imageReadMain, args=(self.eof, self.rawImageQueue))
        # imageReadMain only
        self.droppedFrames = 0
        self.inputvideofile = inputvideofile
        self.input_file_mode = Value('i', 0)

        # TODO if using an input file, this should come from there
        if self.interlaced:
            self.fps = 30.0
        else:
            self.fps = 60.0

        if code_type == CODE_TYPE_ARUCO_4X4:
            self.aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
        elif code_type == CODE_TYPE_ARUCO_5X5: 
            self.aruco_dict = aruco.Dictionary_get(aruco.DICT_5X5_50)
        else:
            self.aruco_dict = aruco.custom_dictionary(5, 4)
        self.aruco_parameters = aruco.DetectorParameters_create()
        # try to reduce false positives
        self.aruco_parameters.maxErroneousBitsInBorderRate = 0.25
        self.aruco_parameters.errorCorrectionRate = 0.4
        # try to reduce CPU loading by increasing minimum marker size
        self.aruco_parameters.minMarkerPerimeterRate = 0.05


        # Try to load calibration
        try:
            with open(self.cal_filename, 'rb') as calfile_fd:
                self.calibration = pickle.load(calfile_fd)
        except FileNotFoundError:
            print("Could not open camera cal file {0}".format(cal_filename))
            self.calibration = None

        # Open video file for capture of raw input if specified
        if len(outputrawfile) > 0:
            out_fourcc = cv2.VideoWriter_fourcc(*'XVID')
            self.output_raw_video = cv2.VideoWriter(
                    outputrawfile, out_fourcc, self.fps, (FRAME_WIDTH, FRAME_HEIGHT))
            print('Recording input video to {:}'.format(outputrawfile))

        self.imageReadProcess.start()

    
    def imageReadMain(self, eof, queue):
        self.eof = eof
        signal.signal(signal.SIGINT, self.imageReadSigint)
        print('Starting image read process')
        self.imageReadInit()
        while (self.eof.value == 0):
            r,image = self.cap.read()
            if r != True:
                self.eof.value = 1 
                print('Could not read')
            else:
                if queue.qsize() < 15:
                    queue.put(image)
                elif not self.input_file_mode.value:
                    self.droppedFrames += 1
                    if (self.droppedFrames % 10) == 0:
                        print('Dropped a total of {:} frames'.format(self.droppedFrames))
                else:
                    # Input file mode.  Just sleep a bit to let other thread catch up
                    time.sleep(0.1)

        print('Image read process exited loop')
        if self.cap.isOpened():
            self.cap.release()
            print('VideoCapture device released')

    def imageReadInit(self):
        # If provided, use input file instead of video capture device
        if len(self.inputvideofile) > 0:
            self.cap = cv2.VideoCapture(self.inputvideofile)
            if not self.cap.isOpened():
                print('Error opening input video file {:}'.format(self.inputvideofile))
                exit(-1)
            else:
                self.input_file_mode.value = 1
                print('Reading input video from {:}'.format(self.inputvideofile))

        else:
            # Set NTSC / video format
            # Seems to be necessary; otherwise the video is screwed up
            with open('/dev/video' + str(self.device), 'wb') as video_fd:
                v4l2_input_bytes = struct.pack('I', 0)
                fourcc_int = struct.unpack('I', self.fourcc.encode('ascii'))[0]
                v4l2_format = struct.pack('IIIIIIIII',
                        V4L2_BUF_TYPE_VIDEO_CAPTURE, # buf type
                        FRAME_WIDTH, # width
                        FRAME_HEIGHT, # height
                        fourcc_int, # pixel format
                        V4L2_FIELD_INTERLACED,
                        0, # padding
                        0, # sizeimage
                        0, # colorspace
                        0)  # private
                padding = max(len(v4l2_format), 200) - len(v4l2_format)
                v4l2_format += bytes(padding) 
                try:
                    ioctl(video_fd, V4L2_VIDIOC_S_INPUT, V4L2_INPUT_0)
                    ioctl(video_fd, V4L2_VIDIOC_S_STD, V4L2_STD_NTSC_M)
                    #ioctl(video_fd, V4L2_VIDIOC_S_FMT, v4l2_format)
                except Exception as e:
                    print('Could not initialize video device: ' + str(e))
                else:
                    print('Initialized video device')

            # Open the device with OpenCV
            self.cap = cv2.VideoCapture(self.device)
            if not self.cap.isOpened():
                print('Error opening device {:}'.format(self.device))
                exit(-1)

            #self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT)
            #print("width")
            self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, FRAME_WIDTH)
            #print("fourcc")
            #self.cap.set(cv2.CAP_PROP_FOURCC, fourcc_int)

            self.input_file_mode.value = 0 

    def imageReadSigint(self, signal, frame):
        print('imageReadProcess: SIGINT received...')
        self.eof.value = 1


    def getRawImage(self):
        if self.eof.value == 0:
            try:
                image = self.rawImageQueue.get(True, 3.0)
            except Empty:
                image = None
                print('Frame queue empty!')
                exit(0)

        else:
            print('Got EOF on frame queue')
            exit(0)

        return image

    def getImage(self):
        # Assume height is divisible by 2
        if self.interlaced or self.input_file_mode.value:
            image = self.getRawImage()
        else:
            # Return two frames for each one captured
            if self.field_even:
                image = self.getRawImage()
                dim = image.shape
                # Can't seem to get EasyCap to return 640x480, only 720x480
                if dim[1] != FRAME_WIDTH or dim[0] != FRAME_HEIGHT:
                    image = cv2.resize(image, (FRAME_WIDTH, FRAME_HEIGHT))
                    dim = image.shape

                self.image_odd = np.copy(image)
                # Even comes first!

                # A - this one *seems* to be a bit less jittery
                if True:
                    self.image_odd[2:dim[0]:2] = self.image_odd[1:dim[0]-1:2]
                    self.image_odd[0] = self.image_odd[1]
                    image[1:dim[0]-1:2] = image[2:dim[0]:2]
                    image[0] = image[1]
                else:
                #B
                    self.image_odd[0:dim[0]:2] = self.image_odd[1:dim[0]:2]
                    image[1:dim[0]:2] = image[0:dim[0]:2]
            else:
                image = self.image_odd
            self.field_even = not self.field_even

        if self.output_raw_video is not None:
            #print(image.shape)
            self.output_raw_video.write(image)

        return image 

    def scanImage(self):
        image_input = self.getImage()
        if image_input is None:
            return [], None

        if self.calibration is not None:
            image_undist = cv2.undistort(
                    image_input, self.calibration['matrix'], self.calibration['distCoeffs'],
                    None, self.calibration['newMatrix'])
            x,y,w,h = roi
            image_undist = image_undist[y:y+h, x:x+w]
        else:
            image_undist = image_input

        if self.code_type == CODE_TYPE_QR:
            image_rgb = cv2.cvtColor(image_undist, cv2.COLOR_BGR2RGB)
            image_zbar = Image.fromarray(image_rgb)
            #scan_data = zbarlight.scan_codes('qrcode', image_zbar)
            image = image_input
        else:
            image_gray = cv2.cvtColor(image_undist, cv2.COLOR_BGR2GRAY)
            corners, ids, rejects = aruco.detectMarkers(
                    image_gray, self.aruco_dict, parameters=self.aruco_parameters)
            image = aruco.drawDetectedMarkers(image_undist, corners)
            try:
                if len(ids) > 0:
                    scan_data = ids[0]
                    if self.debug:
                        print('Pre-filter detections: {:}'.format(scan_data))
                else:
                    scan_data = []
            except TypeError:
                scan_data = []

        #scan_data_filtered = [x for x in scan_data if x in VALID_IDS]
        # Add new scandata, drop oldest
        if len(self.scan_window) >= self.window_frames:
            self.scan_window.pop(0)
        self.scan_window.append(scan_data)

        # See if we have enough hits
        # Flatten to one list
        all_scan_data = [marker for scan_frame in self.scan_window for marker in scan_frame]
        counts = Counter(all_scan_data)
        scan_data_filtered = [x for x in counts if counts[x] >= self.frame_th]

        return scan_data_filtered,image

    def setCodeType(self, code_type):
        if code_type in (CODE_TYPE_QR, CODE_TYPE_ARUCO):
            self.code_type = code_type

    def calibrate(self):
        print("Beginning calibration.  Position board in at least four different viewpoints.")
        allCorners = []
        allIds = []
        decimator = 0
        for i in range(300):

            frame = self.getImage()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            res = cv2.aruco.detectMarkers(gray,dictionary)

            if len(res[0])>0:
                res2 = aruco.interpolateCornersCharuco(res[0],res[1],gray,board)
                if res2[1] is not None and res2[2] is not None and len(res2[1])>3 and decimator%3==0:
                    allCorners.append(res2[1])
                    allIds.append(res2[2])
                    print('+', end='')

                aruco.drawDetectedMarkers(gray,res[0],res[1])

            cv2.imshow('frame',gray)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            decimator+=1

            if decimator%30 == 0:
                print('.', end='', flush=True)

        imsize = gray.shape

        try:
            self.calibration = {}
            err, matrix, distCoeffs = \
                aruco.calibrateCameraCharuco(allCorners,allIds,board,imsize,None,None)
            # TODO do we need to check against some error threshold?
            h,w = imsize[:2]
            newMatrix, roi = cv2.getOptimalNewCameraMatrix(matrix, distCoeffs,(w,h),1,(w,h))
            self.calibration['distCoeffs'] = distCoeffs
            self.calibration['matrix'] = matrix
            self.calibration['newMatrix'] = newMatrix
            self.calibration['roi'] = roi
            with open(self.cal_filename, 'wb') as calfile_fd:
                pickle.dump(self.calibration, calfile_fd)

            print("Calibration succeeded")
        except:
            self.calibration = None
            print("Calibration failed")

        return self.calibration != None

    def getInputFps(self):
        return self.fps
   
    def release(self):

        if self.output_raw_video is not None:
            self.output_raw_video.release()

        self.eof.value = 1 
        # TODO why does join not work here?
        self.imageReadProcess.join(1)
        self.imageReadProcess.terminate()

class Team:
    def __init__(self, name, markers, scan_sound, debug, frame_time_mode=False, fps=0.0, complete_sound=None): 
        self.name = name
        self.all_markers = markers
        self.found_markers = []
        self.all_markers_id_cache = [x['id'] for x in self.all_markers]
        self.found_markers_id_cache = []
        self.start_time = None
        self.end_time = None
        self.debug = debug
        self.scan_sound = scan_sound
        self.complete_sound = complete_sound
        self.frame_time_mode = frame_time_mode
        self.frame_count = 0
        self.found_count = 0
        if self.frame_time_mode:
            self.fps = fps
        else:
            self.fps = 0.0

    def startTimer(self):
        self.start_time = self.getTime()

    def incrementFrameCount(self, count):
        self.frame_count += count

    def foundMarker(self, marker_id):
        if marker_id in self.all_markers_id_cache:
            self.found_count += 1
            # Have we found this one yet?
            if marker_id not in self.found_markers_id_cache:
                marker = [x for x in self.all_markers if x['id'] == marker_id][0]
                self.found_markers.append(marker)
                self.found_markers_id_cache.append(marker['id'])
                self.scan_sound.play()

                # Found all the markers!
                if len(self.found_markers) == len(self.all_markers):
                    self.end_time = self.getTime()
                    if self.complete_sound is not None:
                        self.complete_sound.play()

            elif self.debug:
                # always play the scan sound if we're debugging
                self.scan_sound.play()

    def getFoundMarkers(self):
        return [x['name'] for x in self.found_markers]

    def getElapsedTime(self):
        if self.end_time != None and self.start_time != None:
            return self.end_time - self.start_time
        elif self.start_time != None:
            return self.getTime() - self.start_time
        else:
            return 0

    def getName(self):
        return self.name

    def getTime(self):
        if self.frame_time_mode:
            return self.frame_count/self.fps
        else:
            return time.time()

    def getStatusText(self):
        m,s = divmod(self.getElapsedTime(), 60)
        marker_text = ""
        for marker in self.found_markers:
            marker_text += " " + marker['name']

        if self.debug:
            status_text = '{:}  {:02}:{:02} {:} {:}'.format(
                    self.name, int(m), int(s), self.found_count, marker_text)
        else:
            status_text = '{:}  {:02}:{:02}  {:}'.format(
                    self.name, int(m), int(s), marker_text)

        return status_text

    def reset(self):
        self.end_time = None
        self.start_time = None
        self.found_markers = []
        self.found_markers_id_cache = []

