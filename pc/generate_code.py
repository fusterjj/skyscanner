#!/usr/bin/env python3
"""
Copyright (C) 2017 Joel Fuster - All rights reserved
"""
import segno
import cv2
import cv2.aruco as aruco
import argparse

def generateQRCode(data, filename):
    # segno seems to have more features
    #image = pyqrcode.create(data, version=1)
    #image.png(filename, scale=60)

    image = segno.make_qr(data, error='H')
    image.save(filename, scale=60)
    
def generateMicroQRCode(data, filename):
    image = segno.make_micro(data)
    image.save(filename, scale=60)


def generateArucoCode(data, filename, num_markers=None, num_size=None):
    if num_size == None or num_markers == None:
        aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    else:
        aruco_dict = aruco.custom_dictionary(num_markers, num_size)

    img = aruco.drawMarker(aruco_dict, data, 1000)
    cv2.imwrite(filename, img)

def main():
    #generateQRCodePNG('1', 'test.png')
    parser = argparse.ArgumentParser(description='Generate a code of the specified type')
    parser.add_argument('id', type=int, help='Numeric ID to store in the code')
    parser.add_argument('filename', help='Filename for image.  Only PNG is fully supported.')
    parser.add_argument('--type', dest='codetype', default='aruco', 
            choices=['aruco', 'qr', 'microqr'], 
            help='Type of code to create.  Aruco is default')
    parser.add_argument('--aruco_size', type=int, help='Number of bits per axis for markers')
    parser.add_argument('--aruco_markers', type=int, help='Number of markers')

    args = parser.parse_args()

    if args.codetype == 'microqr':
        generateMicroQRCode(args.id, args.filename)
    elif args.codetype == 'qr':
        generateQRCode(args.id, args.filename)
    else:
        if args.aruco_markers and args.aruco_size:
            generateArucoCode(args.id, args.filename, args.aruco_markers, args.aruco_size)
        else:
            print('Either of aruco_size or aruco_markers not specified; using default dictionary')
            generateArucoCode(args.id, args.filename)

if __name__ == '__main__':
    main()

