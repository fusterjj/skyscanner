#!/bin/sh
ZBAR_VER=1.1.0
ZBAR_TAR=zbarlight-${ZBAR_VER}.tar.gz
PIP=pip3
sudo apt-get install libpython3-dev python3-pip libzbar0 libzbar-dev libv4l-dev
${PIP} install segno pygame

# Too low-level
#${PIP} install pyv4l2

# Does not include video capture support
#${PIP} install opencv-python

#git clone https://github.com/Polyconseil/zbarlight
if [ ! -e ${ZBAR_TAR} ]; then
    wget https://github.com/Polyconseil/zbarlight/archive/${ZBAR_VER}.tar.gz ${ZBAR_TAR}
fi
if [ ! -e zbarlight-${ZBAR_VER} ]; then
    tar xvf ${ZBAR_TAR}
fi

cd zbarlight-${ZBAR_VER} && sudo pip3 install . && cd ..

echo
echo
echo "You must manually build OpenCV to get video support.  Additionally, Aruco support"
echo "requires OpenCV contrib:"
echo "http://docs.opencv.org/trunk/d7/d9f/tutorial_linux_install.html"
echo "https://github.com/opencv/opencv_contrib"

