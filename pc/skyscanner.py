#!/usr/bin/env python3
"""
Copyright (C) 2017 Joel Fuster - All rights reserved
"""
import cv2
import time
import signal
import pygame
import argparse
import glob
import sys
from pygame.mixer import Sound
from skyscannerlib import Team
from skyscannerlib import Scanner
from skyscannerlib import FRAME_WIDTH, FRAME_HEIGHT

SCAN_SOUND = 'scan.wav'
COMPLETE_SOUND = 'complete.wav'
OSD_COLOR = (0, 255, 0)
OSD_FONT = cv2.FONT_HERSHEY_SIMPLEX

#VALID_IDS = (0, 1, 2, 3, 4, 5)
VALID_IDS = (23, 11, 19, 35, 41)
MARKER_NAMES = ('A', 'B', 'C', 'D', 'E')
#MARKER_NAMES = ('M1',)


def quit():
    if scanner:
        scanner.release()

    if output_video != None:
        output_video.release()

    cv2.destroyAllWindows()
    exit(0)

def sigint(signal, frame):
    print('SIGINT received...')
    quit()

def sigusr1(signal, frame):
    global team
    print('SIGUSR1 received, starting')
    team.startTimer()

def sigusr2(signal, frame):
    global team
    print('SIGUSR2 received, resetting')
    team.reset()

def main(args_list):
    global scanner 
    global game_ids
    global output_video
    global team

    signal.signal(signal.SIGINT, sigint)
    signal.signal(signal.SIGUSR1, sigusr1)
    signal.signal(signal.SIGUSR2, sigusr2)

    parser = argparse.ArgumentParser(description='Scans codes/markers')
    parser.add_argument('--device', type=int, help='Video device number to use')
    parser.add_argument('-c', '--calibrate', action='store_true', help='Calibrate the camera instead of scanning')
    parser.add_argument('-d','--debug', action='store_true', help='Activate some debug functions')
    parser.add_argument('-r','--recordraw', default='', help='Record unprocessed video to this file')
    parser.add_argument('-i', '--interlaced', default=False, action='store_true', help='Interlaced mode; higher resolution, 30fps.  Otherwise lower resolution, 60fps')
    parser.add_argument('--infile', default='', help='Use specified input video file instead of video capture.  Takes precedence over specification of capture device.')
    parser.add_argument('-o','--recordoutput', default='', help='Record processed output video to specified AVI file')
    parser.add_argument('-n','--name', default='Team 1', help='Team name')
    args = parser.parse_args(args_list)

    game_markers = []
    for marker_id,marker_name in zip(VALID_IDS, MARKER_NAMES):
        marker = {}
        marker['id'] = marker_id
        marker['name'] = marker_name
        game_markers.append(marker)

    #print(game_markers)

    pygame.mixer.init()
    scan_sound = Sound(SCAN_SOUND)
    complete_sound = Sound(COMPLETE_SOUND)

    # Default to last device if none specified
    if args.device == None:
        video_devs = glob.glob('/dev/video?')
        if len(video_devs) > 0:
            args.device = len(video_devs)-1
        else:
            print('Error, no video devices found')
            exit(-1)

    print('Using video device {:}'.format(args.device))

    scanner = Scanner(args.device, outputrawfile=args.recordraw, interlaced=args.interlaced, 
            inputvideofile=args.infile, debug=args.debug)

    # Are we calibrating?
    if args.calibrate:
        retval = scanner.calibrate()
        if retval:
            exit(0)
        else:
            exit(-1)

    if len(args.infile) > 0:
        frame_time_mode = True
    else:
        frame_time_mode = False
        

    team = Team(args.name, game_markers, scan_sound, args.debug, 
            frame_time_mode=frame_time_mode, fps=scanner.getInputFps(),
            complete_sound=complete_sound)

    # Open video file for capture of processed output if specified
    if len(args.recordoutput) == 0:
        output_video = None
    else:
        #out_fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out_fourcc = cv2.VideoWriter_fourcc(*'X264')
        output_video = cv2.VideoWriter(
                args.recordoutput, out_fourcc, scanner.getInputFps(), 
                (FRAME_WIDTH, FRAME_HEIGHT))
        print('Recording output video to {:}'.format(args.recordoutput))

    scan_count = 0
    start_time = time.time()
    print('Starting scanning...')
    #team.startTimer()
    while True:
        data,image = scanner.scanImage()
        if image is None:
            quit()
        team.incrementFrameCount(1)
        statusText = team.getStatusText()

        cv2.putText(image, statusText, (10, 30), OSD_FONT, 1, OSD_COLOR, 2) 
        cv2.imshow('camera', image)
        if len(data) > 0:
            print('Scanned: ' + str(data))
            for marker_id in data:
                team.foundMarker(marker_id)
                # debug
                #cv2.imwrite('found_' + str(marker_id) + '.png', image)

        if output_video != None:
            output_video.write(image)
        cv2.waitKey(1)

        scan_count = scan_count + 1
        current_time = time.time()
        elapsed_time = current_time - start_time
        if elapsed_time > 2.0:
            fps = scan_count / elapsed_time
            scan_count = 0
            start_time = current_time
            print(statusText + ' FPS: {:.1f}'.format(fps))

if __name__ == '__main__':
    # default to command line args
    main(sys.argv[1:])

